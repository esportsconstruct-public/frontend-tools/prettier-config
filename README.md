# Prettier config for EsportsConstruct

A shareable [Prettier](https://prettier.io/) config for EsportsConstruct projects.

### Publishing a new version

1. Run `npm version patch` (replace `patch` [as necessary](https://docs.npmjs.com/cli/version)) to increase the version number.
2. Run `git push && git push --tags` to push the version commit and tag.
3. Run `npm publish` to publish the new version.
